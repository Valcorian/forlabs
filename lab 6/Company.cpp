#include "Company.h"
#include <iostream>

Company::Company(const std::string& name1){
    name = name1;
}

void Company::print() const {
    std::cout << "Company " << name << std::endl;
}