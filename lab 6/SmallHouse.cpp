#include "SmallHouse.h"
#include <iostream>

SmallHouse::SmallHouse(const std::string& name, const std::string& type, double square)
    : TypeOfHouse(name, type), square(square)
{}


void SmallHouse::print() const {
    std::cout << "square small house: " << square << std::endl;
}