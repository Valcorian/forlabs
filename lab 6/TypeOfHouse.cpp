#include "TypeOfHouse.h"
#include <iostream>

TypeOfHouse::TypeOfHouse(const std::string& name, const std::string& type)
    : Company(name) {
    this->type = type;
}



void TypeOfHouse::print() const {
    Company::print();
    std::cout << "Type house: " << type << std::endl;
}