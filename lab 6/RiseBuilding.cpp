#include "RiseBuilding.h"
#include <iostream>

RiseBuilding::RiseBuilding(const std::string& name, const std::string& type, double square)
    : TypeOfHouse(name, type) {
    this->square = square;
}

void RiseBuilding::print() const {
    std::cout << "Square building: " << square << " m" << std::endl;
}