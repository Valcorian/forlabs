#pragma once
#include <string>

class Company {
private:
    std::string name; 
public:
    Company(const std::string& name1);

    void print() const;
};

