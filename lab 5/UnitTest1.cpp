#include "pch.h"
#include "CppUnitTest.h"
#include "../Project1/Mass.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Mass m1(10);
			Mass m2(12);

			Assert::IsFalse(m1.operator==(m2));
		}

		TEST_METHOD(TestMethod2)
		{
			Mass m1(10);
			Mass m2(12);

			Assert::IsTrue(m1.operator<(m2));
		}

		TEST_METHOD(TestMethod3)
		{
			Mass m1(10);
			Mass m2(12);

			Assert::IsTrue(m2.operator-(m1) == 2);
		}
	};
}
