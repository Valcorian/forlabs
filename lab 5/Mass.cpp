#include "Mass.h"

Mass::Mass(double kg) {
    kilograms = kg;
}

bool Mass::operator>(const Mass& other) const {
    return kilograms > other.kilograms;
}

bool Mass::operator<(const Mass& other) const {
    return kilograms < other.kilograms;
}

bool Mass::operator==(const Mass& other) const {
    return kilograms == other.kilograms;
}

bool Mass::operator!=(const Mass& other) const {
    return kilograms != other.kilograms;
}

Mass Mass::operator+(const Mass& other) const {
    return Mass(kilograms + other.kilograms);
}

Mass Mass::operator-(const Mass& other) const {
    return Mass(kilograms - other.kilograms);
}

Mass Mass::operator*(double value) const {
    return Mass(kilograms * value);
}

Mass Mass::operator/(double value) const {
    return Mass(kilograms / value);
}

double Mass::toTons() const {
    return kilograms / 1000;
}

double Mass::toGrams() const {
    return kilograms * 1000;
}

double Mass::toPounds() const {
    return kilograms * 2.20462;
}

std::ostream& operator<<(std::ostream& out, const Mass& mass) {
    out << mass.kilograms << " kg";
    return out;
}
