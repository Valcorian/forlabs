#pragma once
#include <iostream>

class Mass {
private:
    double kilograms;

public:
    Mass(double kg);

    bool operator>(const Mass& other) const;
    bool operator<(const Mass& other) const;
    bool operator==(const Mass& other) const;
    bool operator!=(const Mass& other) const;

    Mass operator+(const Mass& other) const;
    Mass operator-(const Mass& other) const;
    Mass operator*(double value) const;
    Mass operator/(double value) const;

    double toTons() const;
    double toGrams() const;
    double toPounds() const;

    friend std::ostream& operator<<(std::ostream& out, const Mass& mass);
};