#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>
#include "constants.h"
#include "TetrisField.h"
#include "TetrisFigure.h"
#include "TetrisGame.h"
#include "TetrisScreen.h"
using namespace std;


int main() {


	char command[1000];

	// mode - �������� ��������� �������, con- ������ �������� ����������� ����, ��������� ������/������ � ��������
	sprintf(command, "mode con cols=%d lines=%d", window_width_field, window_height_field);
	system(command);

	srand(time(0));
	TetrisGame game;
	while (1) {
		game.PlayerControl();
		game.moveAutoShape();
		game.Show();
		if (GetKeyState(VK_ESCAPE) < 0) break; // ���� ������ ������� 'ESCAPE', ����� GetKeyState retun ����� ����� � �� ������ �� �����
		Sleep(50);
	}

	return 0;

	return 0;
}