#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>
#include "TetrisScreen.h"
#include "TetrisField.h"
#include "TetrisFigure.h"
#include "constants.h"

using namespace std;
class TetrisGame {
private:
	TetrisScreen screen;
	TetrisField field;
	TetrisFigure figure;
	
public:
	TetrisGame();
	void Show();
	void PlayerControl();
	void moveAutoShape();
};
